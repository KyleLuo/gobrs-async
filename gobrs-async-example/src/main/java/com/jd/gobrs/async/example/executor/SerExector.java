package com.jd.gobrs.async.example.executor;

import com.jd.gobrs.async.example.DataContext;
import com.jd.gobrs.async.task.AsyncTask;

import java.util.Map;

/**
 * @program: gobrs-async
 * @ClassName SerExector
 * @description:
 * @author: sizegang
 * @create: 2022-02-26 16:16
 * @Version 1.0
 **/
public interface SerExector extends AsyncTask<DataContext, Map> {

}
