package com.jd.gobrs.async.task;

/**
 * 结果状态
 * @author sizegang wrote on 2019-11-19.
 */
public enum ResultState {
    SUCCESS,
    TIMEOUT,
    EXCEPTION,
    DEFAULT  //默认状态
}
