package com.jd.gobrs.async.constant;

/**
 * @program: gobrs-async
 * @ClassName GobrsAsyncConstant
 * @description:
 * @author: sizegang
 * @create: 2022-02-10 00:22
 * @Version 1.0
 **/
public class GobrsAsyncConstant {
    public static final String DEFAULT_PARAMS = "DEFAULT_PARAMS";
    public static final Integer DEFAULT_EXPECTION_CODE = 9999; //
}
