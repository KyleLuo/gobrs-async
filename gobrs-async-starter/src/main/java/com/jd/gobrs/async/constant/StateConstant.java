package com.jd.gobrs.async.constant;

/**
 * @program: gobrs-async
 * @ClassName StateConstan
 * @description:
 * @author: sizegang
 * @create: 2022-02-26 18:05
 * @Version 1.0
 **/
public class StateConstant {
    public static final int WORKING = 3;
    public static final int STOP = 4;
    public static final int FINISH = 1;
    public static final int ERROR = 2;
    public static final int INIT = 0;
}
