package com.jd.gobrs.async.callback;

import com.jd.gobrs.async.wrapper.TaskWrapper;

import java.util.List;

/**
 * @author sizegang wrote on 2019-12-27
 * @version 1.0
 */
public class DefaultGroupCallback implements IGroupCallback {
    @Override
    public void success(List<TaskWrapper> taskWrappers) {

    }

    @Override
    public void failure(List<TaskWrapper> taskWrappers, Exception e) {

    }
}
